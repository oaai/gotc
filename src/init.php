<?php
/*
GOTC: Group Order TV Channels
Copyright © 2022 Donatas Klimašauskas

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

const SCM_ZIP = 'channel_list_UE32F4000_1201.scm';
const DATA = 'map-CableD'; // Digital broadcasting channels are here.
const ENTRY_SIZE = 320; // Of a channel; in B.

const ARGC = 3;
const ARG_PN = 0; // Program name.
const ARG_SCM = 1;
const ARG_CSV = 2;
const ARG_ERROR = 2;

const EXIT_ERROR = 1;

function exit_error($msg = null, $status = EXIT_ERROR)
{
    print('error: '.($msg ? $msg : 'run with debugger')."\n");
    exit($status);
}

// Allow only one file name format; maybe later there will be more.
function confirm_filename($file)
{
    if ($file !== SCM_ZIP)
        exit_error('currently, SCM has to be: '.SCM_ZIP, ARG_ERROR);
}

function do_system_archive($program, $options)
{
    system("$program -$options ".SCM_ZIP.' '.DATA.' >/dev/null 2>&1', $status);
    $status && exit_error($program, $status);
}

function is_upload($name)
{
    return strpos($name, 'upload') !== false;
}

set_error_handler(function($undef, $str, $file, $line)
{
    print("$str in $file on line $line\n");

    exit(EXIT_ERROR);
});

// CLI/Web server and upload/download preparation and handling.

if (isset($argc)) { // CLI.
    $upload = is_upload($argv[ARG_PN]);

    if ($argc !== ARGC)
        exit_error("php {$argv[ARG_PN]} ".($upload ? '<SCM> <CSV>' :
        '<CSV> <SCM>'), ARG_ERROR);

    // Note: $argv input/output file indexes are swapped.
    confirm_filename(basename($upload ? $argv[ARG_SCM] : $argv[ARG_CSV]));

    if ($upload) {
        $filescm = $argv[ARG_SCM]; // May be absolute pathname.
        $filecsv = $argv[ARG_CSV];
        $print_formatted_data = 'print_csv';

        copy($filescm, SCM_ZIP); // Will be repacking to it for download.
    } else { // Download.
        $filescm = $argv[ARG_CSV];
        $filecsv = $argv[ARG_SCM];
        $read_formatted_data = 'read_csv';
    }

    unset($upload);
} else { // Web server.
    $filescm = SCM_ZIP;

    if (is_upload($_SERVER['REQUEST_URI'])) {
        $_FILES['filescm']['error'] === UPLOAD_ERR_OK or exit_error('upload');

        confirm_filename($_FILES['filescm']['name']);

        // Move the uploaded file to the script's WD.
        move_uploaded_file($_FILES['filescm']['tmp_name'], $filescm) or
            exit_error('uploaded');

        $print_formatted_data = 'print_json';
    } else { // Download.
        /*
        Send the updated archive file to client and remove it and its
        member. This is OK, when run as a 1 on 1 server/client.

        HTTP POST with [NOW,OFFSET] is done through XHR. Do not
        respond with payload to XHR, since it cannot cause the prompt
        for user to download. Respond with the file content payload
        when there is a GET request (assuming client upholds the order
        of requests and logic).
        */
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $read_formatted_data = 'read_json';
        } else { // HTTP GET.
            header('Cache-Control: no-cache');
            header('Content-Type: application/zip');
            header('Content-Length: '.filesize($filescm));
            header("Content-Disposition: attachment; filename=$filescm");

            readfile($filescm);
            unlink($filescm);

            exit;
        }
    }
}
