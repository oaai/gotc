/*
GOTC: Group Order TV Channels
Copyright © 2022 Donatas Klimašauskas

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

'use strict';

// Data array indexes.
const NOW = 0;
const OFFSET = 1;
const HD = 2;
const NAME = 3;

const TODROP_PRECEDES = 2;
const SHOW_TIMESTAMP = true;
const SHOW_MOVE = 'tomove';
const MARK_MOVED = 'moved';

var message;
var file;
var upload;
var download;
var timestamp;
var move;
var ol;
var xhr;
var data;
var group;
var todrop;
var dropon;
var handle_drop_mode;

function toggle_about(ev)
{
    var about = ev.currentTarget.nextElementSibling;

    about.hidden = !about.hidden;
}

function show_message(msg)
{
    message.innerText = msg;
}

function add_li_data(li, channel, show)
{
    var span;
    var input;

    input = document.createElement('input');
    input.type = 'checkbox';
    li.appendChild(input);
    for (var i = 0; i < show.length; i++) {
	span = document.createElement('span');
	if (show[i] === HD)
	    span.innerText = channel[show[i]] ? 'HD' : '';
	else
	    span.innerText = channel[show[i]];
	li.appendChild(span);
    }

    return li;
}

function set_default_mode()
{
    handle_drop_mode = handle_drop_swap;
    ol.className = 'swap';
}

function switch_mode_listener(remove, add)
{
    ol.removeEventListener('click', remove);
    ol.addEventListener('click', add);
}

function toggle_drop_mode()
{
    var handle_drop_old = handle_drop_mode;

    if (handle_drop_mode === handle_drop_swap) {
	handle_drop_mode = handle_drop_insert;
	ol.className = 'insert';
    } else {
	set_default_mode();
    }

    switch_mode_listener(handle_drop_old, handle_drop_mode);
}

function clear_channels(show)
{
    ol.innerHTML = '';
    timestamp.innerText = show === SHOW_TIMESTAMP ?
	(new Date).toLocaleString() : '';
}

function list_channels()
{
    try {
	var channels = JSON.parse(xhr.responseText);
    } catch (e) {
	show_message('File ' + xhr.responseText);

	return;
    }
    var length = channels.length;
    var li;

    clear_channels(SHOW_TIMESTAMP);
    for (var i = 0; i < length; i++) {
	li = document.createElement('li');
	li.id = i;
	li.dataset.channel = channels[i].slice(NOW, HD); // Leave HD and rest.
	ol.appendChild(add_li_data(li, channels[i], [NOW, HD, NAME]));
    }
    ol.addEventListener('click', handle_pick);

    download.disabled = '';
    show_message(length + ' channels. They are ready to be moved');
}

/*
Since at the server the changed channels are being detected by their
position in the GUI, allowing multiple consecutive downloads could end
up with channels file not in sync with the order in the GUI or even
with some channels having the same order number.
*/
function disable_download()
{
    download.disabled = true;
    clear_channels();
}

/*
In order to get the browser to prompt user for file saving, HTTP GET
is achieved, since XHR response, it seems, does not trigger that. The
page is not reloaded, so the sense of an "app" is maintained.
*/
function show_save_file()
{
    location.pathname = 'download.php';
    show_message('Save the new channels list file');
}

// Get server-relevant data, format and return as JSON string.
function get_channels()
{
    var lis = ol.getElementsByTagName('li');
    var json = '';

    for (var i = 0; i < lis.length; i++)
	json += '[' + lis[i].dataset.channel + '],';

    return '[' + json.slice(0, -1) + ']';
}

/*
To upload a file, PHP requires that the FORM element would be:

<form enctype="multipart/form-data" method="POST" action="upload.php">
  <input type="file" name="filescm">
  <input type="submit" value="Upload" disabled>
</form>

To have the browser to send the HTTP request like that through XHR
object, the FormData class and its append method are used.

JSON data from client to server can as well be sent with the same
content type encoding (and received with PHP through $_POST
superglobal). Setting

xhr.setRequestHeader('Content-Type', 'application/json');

then sets the "external" content type to it, instead of

Content-Type: multipart/form-data; boundary=---...

which PHP does not take.
*/

function set_xhr_data(action, onload, key, value)
{
    xhr.open('POST', action + '.php');
    xhr.onload = onload;
    data.append(key, value);
}

function handle_submit(ev)
{
    ev.preventDefault();

    xhr = new XMLHttpRequest();
    data = new FormData();
    if (ev.target.value === 'Upload')
	set_xhr_data('upload', list_channels, 'filescm', file.files[0]);
    else
	set_xhr_data('download', show_save_file, 'channels', get_channels());
    xhr.send(data);
}

function enable_upload()
{
    file.removeEventListener('change', enable_upload);
    upload.disabled = '';
}

function get_top()
{
    return document.getElementById(group[0]);
}

function present_move(show)
{
    var display = '';
    var method = 'remove';

    if (show === SHOW_MOVE) {
	display = 'block';
	method = 'add';
    }

    move.style.display = display;
    group.forEach(function(id)
		  {
		      document.getElementById(id).classList[method](SHOW_MOVE);
		  });
}

function get_index(li)
{
    return Array.prototype.indexOf.call(ol.childNodes, li);
}

function show_move()
{
    var top = get_index(get_top()) + 1;
    var more = group.length - 1;

    move.firstElementChild.innerText = more ? top + '-' + (top + more) : top;
    present_move(SHOW_MOVE);
}

function prepare_pick()
{
    switch_mode_listener(handle_drop_mode, handle_pick);
    present_move();
}

function handle_pick(ev)
{
    // If mode changed before the first click, remove second click handler.
    ol.removeEventListener('click', handle_drop_mode);

    var current = ev.target;
    var previous;

    switch (current.nodeName) {
    case 'INPUT': // User is (un)setting the checkbox.
	return;
    case 'LI': // Movable element.
	break;
    default: // Get the movable element.
	current = current.parentNode;
	break;
    }

    switch_mode_listener(handle_pick, handle_drop_mode);

    // Single LI element move.
    if (!current.firstElementChild.checked) { // Checkbox INPUT element.
	group = [current.id];
	show_move();

	return;
    }

    // Group move of LI elements; at least 1 checked.

    // Get to the top most checked element.
    while ((previous = current.previousElementSibling))
	if (previous.firstElementChild.checked)
	    current = previous;
	else
	    break;

    // At the top. Get all consecutively checked elements.
    group = [current.id];
    while ((current = current.nextElementSibling))
	if (current.firstElementChild.checked)
	    group.push(current.id);
	else
	    break;

    show_move();
}

/*
Picking (similarly but not the same as dragging of DnD) and dropping
channel LI elements may be done in one of the following modes. The LI
(or consecutive group of them) on which a picked LI is dropped: on
Swap goes to the picked LI's old place, giving its place to the
dropped LI; on Insert is moved up 1 LI if the dropped LI comes from
above (precedes), and down 1 LI otherwise.
*/

function prepare_drop(ev)
{
    todrop = get_top();
    dropon = ev.target.nodeName == 'LI' ? ev.target : ev.target.parentNode;

    prepare_pick();
}

// LIs already have to take the new spots in the DOM.
function mark_moved()
{
    ol.childNodes.forEach(function(li)
			  {
			      get_index(li) != li.id ?
				  li.classList.add(MARK_MOVED) :
				  li.classList.remove(MARK_MOVED);
			  });
}

function handle_drop_swap(ev)
{
    prepare_drop(ev);

    var toreplace = [dropon.id];
    var reference = dropon;
    var length = group.length;

    /*
    First, dropon group has to be got; same length as todrop group. If
    the dropon group is too short (at the end of the list), elongate
    it by adjusting up (there is enough to equalize). Then, detect
    overlap. If overlapping, abort the drop.
    */

    while (toreplace.length < length)
	if (reference && (reference = reference.nextElementSibling))
	    toreplace.push(reference.id);
	else
	    toreplace.unshift((dropon = dropon.previousElementSibling).id);

    for (var i = 0; i < length; i++)
	for (var j = 0; j < length; j++)
	    if (group[i] === toreplace[j]) // Overlap.
		return;

    // Two. Both are neighbors; swapping the two is the same as inserting.
    if (todrop.nextElementSibling === dropon) {
	handle_drop_insert(ev);

	return;
    }

    // Group. Get references for and do consecutive swaps of twos.
    for (var i = 0, reft, refd; i < length; i++) {
	reft = todrop.nextElementSibling;
	refd = dropon.nextElementSibling;
	ol.replaceChild(todrop, dropon);
	ol.insertBefore(dropon, reft);
	todrop = reft;
	dropon = refd;
    }

    mark_moved();
}

function handle_drop_insert(ev)
{
    prepare_drop(ev);

    var length = group.length;

    if (dropon.compareDocumentPosition(todrop) & TODROP_PRECEDES)
	for (var i = 0; i < length; i++) // todrop top replaces dropon.
	    if (!(dropon = dropon.nextElementSibling))
		break;

    for (var i = 0; i < length; i++) {
	todrop = document.getElementById(group[i]); // Top re-got; convenience.
	if (todrop === dropon)
	    break;
	ol.insertBefore(todrop, dropon);
    }

    mark_moved();
}

function init()
{
    message = document.getElementById('message');
    file = document.querySelector('input[type=file]');
    upload = document.querySelector('input[value=Upload]');
    download = document.querySelector('input[value=Download]');
    timestamp = document.getElementById('timestamp');
    move = document.getElementById('move');
    ol = document.getElementsByTagName('ol')[0];

    document.querySelector('p').addEventListener('click', toggle_about);
    file.addEventListener('change', enable_upload);
    upload.addEventListener('click', handle_submit);
    download.addEventListener('click', handle_submit);
    download.addEventListener('click', disable_download);
    document.querySelectorAll('input[type=radio]').
	forEach(function(radio, index)
		{
		    radio.addEventListener('change', toggle_drop_mode);
		    if (!index) // After F5 or ^R could be different; reset.
			radio.checked = true;
		});
    move.lastElementChild.addEventListener('click', prepare_pick);

    set_default_mode();
}

window.addEventListener('load', init);
