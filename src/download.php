<?php
/*
GOTC: Group Order TV Channels
Copyright © 2022 Donatas Klimašauskas

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

require './init.php';

const PADDING = [null, null]; // For array index == channel number.

const CHANNEL_NUMBER_SIZE = 2; // B.
const ENTRY_CHECKSUMABLE_SIZE = ENTRY_SIZE - 1; // 1 B checksum at the end.
// Read only constant data; channel number at the start is new on the move.
const ENTRY_MOVE_SIZE = ENTRY_CHECKSUMABLE_SIZE - CHANNEL_NUMBER_SIZE;

// read_{csv,json} mirrors print_{csv,json} of upload.php.

function read_csv()
{
    global $filecsv, $channels;

    $fp = fopen($filecsv, 'rt');
    $channels = [PADDING];

    // All fields are strings; PHP's auto-cast handles this.
    while (($fields = fgetcsv($fp)))
        $channels[] = $fields;

    fclose($fp);
}

function read_json()
{
    global $channels;

    $channels = json_decode($_POST['channels']) or exit_error('JSON');
    array_unshift($channels, PADDING);
}

$read_formatted_data();

/*
Find moved channel, get to its entry, write the new channel number,
calculate and write the checksum.

Checksum is addition of all bytes of an entry into a char (1 B) and
storing it at the last byte of the entry. Implemented by adding bytes
of the checksum-able part in PHP's int (signed) and taking its
LSB. Theoretically, this could overflow, and result in the conversion
to a float, only on architecture with 16 b int (short). Anything above
that should be safe.

The below f{seek,read,write,seek,write} sequence is rather complex for
the efficiency (and longevity of SSD) of minimal write of only the
changed data and when the data is at the beginning and end of a
block. If anything would be to be written in between, the better
approach would probably be just to read the whole block into memory,
modify it and then write the whole block back.

But first, save the original timestamp of the data file and reset it
after the modification, plus one second, since in the SCM it has a
very far into the future timestamp and the ZIP freshening of the
archive is to be achieved.
*/

$timestamp = filemtime(DATA);
$fp = fopen(DATA, 'r+b');

foreach ($channels as $new => list($now, $offset))
    if ($new != $now) {
        // Get to immutable data and get it.
        !fseek($fp, $offset * ENTRY_SIZE + CHANNEL_NUMBER_SIZE);
        $data = fread($fp, ENTRY_MOVE_SIZE);

        // Calculate the checksum.
        $new = pack('v', $new);
        $checksumable = $new.$data;
        $checksum = 0;
        for ($i = 0; $i < ENTRY_CHECKSUMABLE_SIZE; $i++)
            $checksum += ord($checksumable[$i]);

        /*
        File pointer is at the checksum byte. Write the new one and
        get to entry start and write the new channel number.
        */
        fwrite($fp, pack('C', $checksum));
        !fseek($fp, -ENTRY_SIZE, SEEK_CUR);
        fwrite($fp, $new);
    }

fclose($fp);

// Update the modified member of the archive and remove the member.
touch(DATA, $timestamp + 1);
do_system_archive('zip', 'f');
unlink(DATA);

// CLI: move the archive from WD to user specified pathname.
isset($argc) and rename(SCM_ZIP, $filescm);

exit;
