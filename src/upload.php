<?php
/*
GOTC: Group Order TV Channels
Copyright © 2022 Donatas Klimašauskas

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

require './init.php';

/*
If run as a CLI script, output CSV to stdout; send JSON data to client
otherwise.
*/

function print_csv()
{
    global $filecsv, $channels;

    $fp = fopen($filecsv, 'wt');

    foreach ($channels as $fields)
        fputcsv($fp, $fields);

    fclose($fp);
}

/*
Since the array element with the index 0 is removed, the one-based
array is non-sequential and gets encoded to JSON as an object, rather
than an array, with key/value pairs having the index as a key (the
"now" value repeated). To avoid this, the array re-indexing is done.
*/
function print_json()
{
    global $channels;

    $channels = array_values($channels); // Re-index to start from 0, not 1.
    header('Content-Type: application/json');
    print(json_encode($channels));
}

/*
A channel name may still have null byte(s) after the character
encoding conversion (e.g., the actual null bytes encoded as text and
interspersed). Replace with a space.
*/
function filter_name($name)
{
    $name = iconv('UTF-16BE', 'UTF-8', $name); // UTF-8 has all; JSON fit.

    return strpos($name, "\0") !== false ?
        str_replace("\0", ' ', $name) :
        $name;
}

// Is ZIP archive OK?
do_system_archive('unzip', 'tqq');
// Overwrite if we have multiple uploads.
do_system_archive('unzip', 'oq');

/*
Extract channel displayed number -- the "now" number, its name and HD
status, track its entry offset. Use the now number as an index for an
array of currently displayed numbers. "0" marks the end of the
occupied entries.
*/

$channels = []; // In the order displayed by TV.
$offset = 0;
$fp = fopen(DATA, 'rb');

do {
    $data = fread($fp, ENTRY_SIZE);
    $entry = unpack('vnow/x14/Chd/x47/A200name', $data);
    $now = $entry['now'];

    $channels[$now] = [
        $now, // Detect moved when != to index, for efficient read/write.
        $offset++,
        $entry['hd'], // Seems only LSb switching; treat it as bool.
        filter_name($entry['name']),
    ];
} while ($now);

fclose($fp);

/*
Before ksort(), the last element has now == 0, which is used to detect
when empty entries start. Other elements are in the order they were
read.

After ksort(), it would be the first element and care would have to be
taken when swapping channel lines (especially CSV): the empty entry
would have to remain the first line of the file. Thus, removing it
from output and "adding" to input is the safer approach.
*/
array_pop($channels);
ksort($channels);

$print_formatted_data();

exit;
