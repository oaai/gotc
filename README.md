# About

**GOTC: Group Order TV Channels**

Export channels list file from TV. Select the channels list file
and upload it. Digital broadcasting channels will be listed (the
date and time of the upload will be shown). First touch/click
picks and second touch/click drops a channel or channels in one
of the modes: Swap (dropped channel changes a place with that on
which it was dropped; it is not possible to swap within a group
of channels) or Insert (channels move around the dropped one).
When you are done, download the updated channels list file (you
may print it before the download; after downloading, the list
will be cleared). Import it to TV.

# TV

## Works

Samsung UE32F4000.

## Could Work

Samsung UE**F*000 (and some other models, released at the similar
time), but code would need to be modified to try it with any of these
devices. (Constant's `SCM_ZIP`, at `init.php`, value changed to the
file name, which TV to be tried exports.)

# Design

The program is composed of back-end and front-end, communicating to
achieve a desired result. For the same purpose, only back-end could be
used, albeit with more manual work, but with the same result.

The back-end below is referred to as server and the front-end as
client.

Server does the main control and processing, while the client provides
the convenience of GUI manipulation of a list of data -- drag and drop
experience, in a portable way. Server is implemented in PHP; client --
HTML5.

This server/client arrangement is meant to be run on the same computer
with 1 on 1 mapping, i.e. one server serves one client.

This server/client arrangement is not meant to be run on a public
server, but it could be made if a need arises. In such a case, PHP
security must be enhanced, multiple upload files and their content
handling set up, etc.

Uploaded file overwrites the one on the server. After downloading, it
is removed from the server.

# Requirements

## Programs

1. GNU/Linux OS.
1. PHP v7.3+.
1. Mozilla Firefox v91+ or similar, modern web browser (optional).

## Files

### Server

```
download.php
init.php
upload.php
```

### Client (optional)

```
favicon.ico
gotc.css
gotc.js
index.html
```

# Usage

## CLI

Specify the SCM file, which is exported from TV, to modify the order
of stored in it channels, and CSV file, where the modification, with a
text editor, of the channels' order will be done, as in step #1.

When the CSV file is opened with a text editor, it will have the list
of current channels: one channel per one line. The text after the last
comma is the name of the channel (excluding double quotes, if any).
Move the line(s) to attain the desired order of them. Do not change
anything in the lines. Only whole line(s) can change place.

When done, specify the CSV file with the modified order of channels
and where to save the new SCM file with them and its name (it has to
be the same name as TV exports), to be imported to TV, as in step #2.

1. `php upload.php <input-file.scm> <output-file.csv>`
2. `php download.php <input-file.csv> <output-file.scm>`

## GUI

### Web Server

Serves the GUI files and processes upload and download data. Run this
at the `src` directory of the program:

`php -S localhost:8080`

Data streams between client (`<`) and server (`>`) are depicted as
shell I/O streams and are as follows:

`upload.php <SCM >JSON`\
`download.php <JSON >SCM`

### Web Browser

Uploads and downloads SCM files from client perspective and SCM files
and JSON from server perspective. Provides and handles user
interaction with the data. Enter this to the browser's address field:

`http://localhost:8080/`

# Copyright

Copyright © 2022 Donatas Klimašauskas

# License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.

Full license is at the COPYING file.
